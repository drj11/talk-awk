# The AWK Programming Language

Heralded by some[1] as "235 times faster than hadoop",
AWK is a programmable text processing tool that
has been packaged with Unix, since 1979 ish,
and has been in all major Unix standards since.
AWK grows on the `grep` and `sed` tools,
borrowing some of their syntax and style in a tool that is
almost as concise but fully programmable.
AWK is good at cutting through text-based datasets at blazing speed.
I'll introduce you to some its ways.

REFERENCES

[1] https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html

# Slide Notes

## Introduction

AWK is a programmable text processing tool.
The `awk` program comes as standard with Unix, since 1979 ish,
and has been in all major Unix standards.
Named after its creators: Aho, Weinberger, Kernighan.
Grows on the `grep` and `sed` tools.

## Some suggestions

Ad hoc feedback from WF 2019-02-22:

motivate with examples (relevant to The Academy).

mention good streaming with unix pipe and low memory.

point up regular expressions.

be more explicit about `grep` and `sed`?

## Why tho?

it's the original map/reduce hadoop

Good availability:
`awk` comes as standard on basically all Unix systems (including OS X),
and is available on Windows using the Windows Subsystem for Linux.

## Invoking awk

Invoking awk:

    awk prog file1 ...

`awk` processes its input as a series of *records*.
Usually, each record from the input is terminated by a newline.

An AWK program consists of a series of *pattern* *action* statements:

    /pattern/ { print "matched" }

## The builtin loop

When it runs the program,
`awk` steps through its input one record at a time,
and cycles through the pattern/action statements.
The pattern is matched against the current record, and
if it matches, the corresponding action is executed.

The simplest action is `print` which prints the current record,
and in this case the action can be left out.
Pattern, no action:

    /pattern/

Prints every record that matches `/pattern/`.

These patterns are regular expressions,
and POSIX says they are POSIX Extended Regular Expressions,
which means that parentheses work.

These are the same regular expressions that `egrep` uses.

## Fields

It's quite often useful to miss the pattern out
(in which case it matches on every line):

    { print $2 }

This example print the second _field_ of each line of input.
`awk` splits each record into a number of *fields*,
accessible with `$1`, `$2`, and so on.
`$0` is the entire record.

## Quote from shell

`awk` programs often contain characters that are
special to the shell, such as space, double quote, and dollar;
they need protecting from the shell when you
put the `awk` program on the command line.
Single quotes come in handy:

    awk '{ print $2 }' infile

You can also put the awk program in a file and use the `-f` option:

    awk -f progfile infile

## Numbers

`awk` has numbers and numeric expressions.
`NR` is a special variable that stores
the number of records processed so far (including the current one);

    awk 'NR > 1' infile

prints all the lines after the first one.

    NR % 3 == 0    # print every 3rd line

    $1 == $5       # print records where 1st and 5th fields match

    rand() < 0.01  # print a random 1% of all lines

In these examples the pattern is a comparison instead of
a regular expressions. 
The behaviour is similar: when the comparison is true the action happens.

Note that the last example uses floating point numbers.
In `awk`, all numbers are floating point,
with care taken to print integers exactly.
Just like JavaScript.

The example is a cute way of taking a 1% sample a file,
which is a great way of reducing a large file to a smaller one
so that your development goes faster, but it comes with a
warning:

That `rand()` returns the same sequence each time, so re-running
it will give exactly the same "random" sample.

You can seed the pseudo-random number generator with `srand()`,
but there's probably not enough entropy.
Short version: Do not expect high quality random numbers.

## Variables

Useful for storing and summing and stuff:

    { s += $5 }

If you run this `awk` program, it won't be very useful.
You need to print `s` after the entire input has been read.
`awk` has a special pattern, `END`,
that matches only after the last record has been processed:

    { s += $5 }
    END { print s }

## Splitting

Normally fields are split by whitespace;
you can use the `-F` option to change it.

    awk -F: '{ print $3 }' /etc/passwd

The ASCII unit separator is my favourite separator,
but is a bit hard to type.
You can in fact press Ctrl-Underscore and have this character
appear directly in your script, but it seems unwise to
advertise scripts with non-printable characters in them.
So here we use `printf`.

    awk -F$(printf '\037') '$1 == 85' Scales.txt

## loops

separate file so that it is one word per line:

    awk '{for(i=1; i<=NF; ++i) {
      print $i
    }'

Of note:

3-part loop syntax borrowed from C.

`NF` is a magic variable that holds the number of fields in
the current record.

The loop needs to start at 1 and finish at NF.

`$i` is the i'th field.
You can use variables with `$` and in fact,
`$` is just a (high precedence) operator that takes any expression.


## built in functions

We're already seen `rand()`.
These are also fun:

    length($0)

    index(haystack, needle)

    substr($0, 10)

    gsub(/:/, ".")

    split($1, result, "/")


## More fun stuff

    if($1 ~ /drj/) { stuff }

    print $(NF-1)

    $2 = "blank"

    a[$1] += 1

## What is it good for?

Short one liners. Low memory, streaming.


## Further Reading

The AWK Programming Language, Aho, Kernighan, Weinberger.

The UNIX Programming Environment.

The Practice Programming.

## Acknowledgements

Thanks to The Sensible Code Company and
The University of Sheffield for
providing time and hosting to develop this talk.

## two puzzles
